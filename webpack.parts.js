const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
exports.devServer = ({ host, port } = {}) => ({
    devServer: {
        contentBase: './build',
        publicPath: '/',
        historyApiFallback: true,
        // stats: 'normal',
        host, // Defaults to `localhost`
        port, // Defaults to 8080
        overlay: {
            errors: true,
            warnings: false,
        },
        
    },
    
});
exports.clean = (path) => ({
    plugins: [
        new CleanWebpackPlugin([path]),
    ],
});
exports.page = ({
    path = '',
    template = require.resolve(
        'html-webpack-plugin/default_index.ejs'
    ),
    title,
    entry,
} = {}) => ({
    entry,
    plugins: [
        new HtmlWebpackPlugin({
            filename: `${path && path + '/'}index.html`,
            template,
            title,
            inject:false,
        }),
    ],
});
exports.lintJavaScript = ({ include, exclude, options }) => ({
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                enforce: 'pre',

                loader: 'eslint-loader',
                options,
            },
        ],
    },
});
exports.loadJavaScript = ({ include, exclude }) => ({
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
  
                loader: 'babel-loader',
                options: {
                    // Enable caching for improved performance during
                    // development.
                    // It uses default OS directory by default. If you need
                    // something more custom, pass a path to it.
                    // I.e., { cacheDirectory: '<path>' }
                    cacheDirectory: true,
                },
                
            },
        ],
    },
});