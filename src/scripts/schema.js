import pubsub from 'pubsub';
import keyboard from 'keyboard';
import rivets from 'rivets';
import highcharts from 'highcharts';
import prettyByte from 'pretty-byte';
import prettyBps from 'pretty-bps';
import humanizeDur from 'humanize-duration';
import { setTimeout } from 'core-js/library/web/timers';
/* eslint-disable no-undef */
window.$ = $;
window.jQuery = $;
// window.rivets = rivets;
/* eslint-enable no-undef */
var init = function (factory){
    let trns = factory.trns;
    let globals = factory.options;
    var direction = (globals.config.lang === 'fa')? 'rtl' : 'ltr';
    var gWidth  = '100%';
    var gHeight  = '100%';
    var mainAccordion = document.createElement('div');
    mainAccordion.setAttribute('id', globals.markup.accordionBaseID);
    mainAccordion.setAttribute('dir', direction);
    
    //prepare module variables
    let moduleArray = Object.keys(globals.modules).map(function(key) {
        return (globals.modules[key]) ? String(key) : 0;
    }).filter(function(x) {
        return typeof x !== 'number';
    });
    moduleArray.forEach(function(module){
        let header = document.createElement('h3');
        header.setAttribute('id', globals.markup.accordionBaseID+'-header-'+ String(module));
        header.innerText = (direction == 'rtl') ? trns.t('informationTitle') + ' '+ trns.t(module) : trns.t(module) +' '+ trns.t('informationTitle'); 
        header.align = (direction == 'rtl') ? 'right': 'left';
        let contentPanel = document.createElement('div');
        contentPanel.setAttribute('id', globals.markup.accordionBaseID+'-content-'+ String(module));
        contentPanel.setAttribute('class', 'row');
        contentPanel.direction = direction;
        mainAccordion.appendChild(header);
        mainAccordion.appendChild(contentPanel);
    });
    var container = document.getElementById(globals.targetId);

    container.appendChild(mainAccordion);
    generateTemplates(factory);
    
};
var generateTemplates = function(factory){
    let direction = (factory.options.config.lang == 'fa')? 'rtl' : 'ltr';
    let float = (direction == 'rtl') ? 'float-left': 'float-right';
    let tableTxt = (direction == 'rtl') ? 'text-right': 'text-left';
    let floatPills = (direction == 'rtl') ? 'float-right': 'float-left';
    
    let trns = factory.trns;
    let globals = factory.options;

    rivets.components['host-info-comp'] = {
        template: () =>    '<div id="host-info">'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('hostName') +': </span> <span id="host-hostName" class="badge badge-secondary"> {data.hostName}</span> </div><span> </span>'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('domainName') +': </span> <span id="host-domainName" class="badge badge-secondary"> {data.domainName} </span> </div><span> </span>'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('dnsServers') +': </span> <span rv-each-dns="data.dnsServers" id="host-dnsServers" class="badge badge-secondary"> {dns} </span> </div><span> </span>'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('defaultGateway') +': </span> <span id="host-defaultGateway" class="badge badge-secondary"> {data.defaultGateway} </span> </div><span> </span>'+
            '</div>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };
    rivets.components['os-info-comp'] = {
        template: () =>    '<div id="os-info">'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('vendor') +': </span> <span id="os-vendor" class="badge badge-secondary"> {data.vendor}</span> </div><span> </span>'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('name') +': </span> <span id="os-name" class="badge badge-secondary"> {data.name} </span> </div><span> </span>'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('version') +': </span> <span id="os-version" class="badge badge-secondary"> {data.version} </span> </div><span> </span>'+
            '<div class="'+floatPills+' inline-info"> <span class="host-info ">'+ trns.t('architecture') +': </span> <span id="os-architecture" class="badge badge-secondary"> {data.architecture} </span> </div><span> </span>'+
            '</div>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };

    rivets.components['cpu-info-comp'] = {
        template: () =>    '<div class="row" id="cpu-info">'+
            '<div class="cpu-info list-group  col-lg-4">'+
                '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center "> <span>'+ trns.t('model') +': </span> <span dir="ltr" data-toggle="tooltip" data-placement="top" rv-title="data.model" id="cpu-model" class="badge badge-dark badge-pill '+ float +' truncate"> {data.model}</span> </div>'+
                '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center "> <span>'+ trns.t('architecture') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="cpu-architecture"> {data.architecture} </span> </div>'+
                '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center "> <span>'+ trns.t('physicalCore') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="cpu-physicaCode"> {data.physicalCore} </span> </div>'+
            '</div>'+
            '<div class="cpu-info list-group  col-lg-4">'+
                '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center "> <span>'+ trns.t('logicalCore') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="cpu-logicalCore"> {data.logicalCore} </span> </div>'+
                '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center "> <span>'+ trns.t('uptime') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="cpu-uptime"> {data.uptime | humanize} </span> </div>'+
                '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center "> <span>'+ trns.t('load') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="cpu-load"> {data.load} %</span> </div>'+
            // '<div class="cpu-info list-group-item d-flex justify-content-between align-items-center " rv-each-core="data.cores"> <span>'+ trns.t('load')  +'{ %core%}: </span> <span class="badge badge-dark badge-pill '+ float +'" id="cpu-load"> {core.load} </span> </div>'+
            '</div>'+
            '<div class="cpu-info  col-lg-4 list-group" id="cpu-chart">  </div>'+
            '</div>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };
    rivets.components['ram-info-comp'] = {
        template: () =>    '<div class="row" id="ram-info">'+
        '<div class="ram-info-panel list-group  col-lg-4">'+
            '<div class="ram-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('capacity') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="ram-capacity"> {data.capacity | byte} </span> </div>'+
            '<div class="ram-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('used') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="ram-used"> {data.used | byte} </span>  </div>'+
            '<div class="ram-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('free') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="ram-free"> {data.free | byte} </span> </div>'+
        '</div>'+
        '<div class="ram-info-panel list-group  col-lg-4">'+
            '<div class="ram-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('swapSize') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="ram-swapSize"> {data.swapSize | byte} </span> </div>'+
            '<div class="ram-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('swapUsed') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="ram-swapUsed"> {data.swapUsed | byte} </span> </div>'+
            '<div class="ram-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('swapFree') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="ram-swapFree"> {data.swapFree | byte} </span> </div>'+        
        '</div>'+
        '<div class="ram-info col-lg-4 list-group d-flex justify-content-between align-items-center" id="ram-chart">  </div>'+
        '</div>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };

    rivets.components['storages-info-comp'] = {
        template: () =>  '<div class="row" id="storage-info">'+
        '<div  class="col"  rv-each-storage="data">'+
            '<div style="max-width:370px;" class="list-group storages-info-panel">'+
                '<h3 dir="'+ direction +'" class="list-group-item text-center">'+ trns.t('storage') +' {%storage% | indexifier}</h3>'+
                '<div class="storages-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('capacity') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="storages-capacity"> {storage.capacity | byte}</span> </div>'+
                '<div class="storages-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('used') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="storages-used"> {storage.used | byte} </span>  </div>'+
                '<div class="storages-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('free') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="storages-free"> {storage.free | byte}  </span></div>'+
                '<div class="storages-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('fileSystem') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="storages-fileSystem"> {storage.fileSystem}</span> </div>'+     
                '<div class="storage-info list-group-item" rv-id="%storage% | storageFrmttr">  </div>'+
            '</div>'+
        '</div>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };
    rivets.components['nics-info-comp'] = {
        template: () =>    '<div class="row" id="nics-info">'+
        '<div class="col"   rv-each-nic="data">'+
            '<div style="max-width:370px;" class="list-group nics-info-panel">'+
                '<h3 dir="'+ direction +'" class="list-group-item text-center">'+ trns.t('nic') +' {%nic% | indexifier}</h3>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('name') +': </span> <span dir="ltr" class="badge badge-dark badge-pill '+ float +'" id="nics-name"> {nic.name }</span> </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('macAddress') +': </span> <span  class="badge badge-dark badge-pill '+ float +'"id="nics-macAddress"> {nic.macAddress }</span>  </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('speed') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="nics-speed"> {nic.speed | bps  } </span> </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('IPv4') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="nics-IPv4"> {nic.IPv4}</span> </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('mask') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="nics-mask"> {nic.mask }</span> </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('gateway') +': </span> <span class="badge badge-dark badge-pill '+ float +'" id="nics-gateway"> {nic.gateway }</span>  </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('in') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="nics-in"> {nic.in | bps } </span> </div>'+
                '<div class="nics-info list-group-item d-flex justify-content-between align-items-center"> <span>'+ trns.t('out') +': </span> <span class="badge badge-dark badge-pill '+ float +' byte-info" id="nics-out"> {nic.out | bps} </span> </div>'+    
            '</div>'+
        '</div>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };
    
    rivets.components['functions-info-comp'] = {
        template: () =>    '<table class="table table" id="functions-info">'+
            '<thead>'+
                '<tr class="row '+tableTxt+'"><th class="col-2" scope="col">#</th><th class="col-5" scope="col">'+ trns.t('functions') +'</th><th class="col-5" scope="col">'+ trns.t('resources') +'</th></tr>'+
            '</thead>'+
            '<tbody>'+
            '<tr class="row  '+tableTxt+'" rv-each-function="data">'+
              '<th class="col-2" scope="row">{%function% |indexifier}</th>'+
              '<td class="col-5">{function.name}</td>'+
              '<td class="col-5">{function.resources}</td>'+
            '</tr>'+
            
          '</tbody>'+
        '</table>',
       
        initialize: (el, attrs) => {
            return { data: attrs.data };
        },
    };
    rivets.formatters.byte = function(value){
        return prettyByte(parseInt(value));
    };
    rivets.formatters.bps = function(value){
        return prettyBps(parseInt(value));
    };
    rivets.formatters.storageFrmttr = function(value){
        return 'storage-chart-'+value;
    };
    rivets.formatters.indexifier = function(value){
        return value+1;
    };
    rivets.formatters.humanize = function(value){
        let dur = parseInt(value);
        let result;

        
        let options= {  // '1 year, 1 month, 5 days'
            unitMeasures: {
                y: 31557600000,
                mo: 2629800000,
                w: 604800000,
                d: 86400000,
                h:3600000,
                m:60000,
                s: 1000,
            },
            round: true,
            units: ['y', 'mo', 'd', 'h', 'm', 's'],
            language: factory.options.config.lang,
            languages: {
                en: {
                    y: function (c) { return 'year' + (c === 1 ? '' : 's'); },
                    mo: function (c) { return 'month' + (c === 1 ? '' : 's'); },
                    w: function (c) { return 'week' + (c === 1 ? '' : 's'); },
                    d: function (c) { return 'day' + (c === 1 ? '' : 's'); },
                    h: function() { return ''; },
                    m: function() { return '\''; },
                    s: function() { return '"'; },
                    ms: function() { return 'ms'; },
                    decimal: '.',
                },
                fa: {
                    y: function (c) { return 'سال'; },
                    mo: function (c) { return 'ماه'; },
                    w: function (c) { return 'هفته'; },
                    d: function (c) { return 'روز'; },
                    h: function() { return ''; },
                    m: function() { return '\''; },
                    s: function() { return '"'; },
                    ms: function() { return 'ms'; },
                    decimal: '.',
                },
            },
        };

        
        result = humanizeDur(dur, options); 
        return result;
    };

};



export {
    init,
};   