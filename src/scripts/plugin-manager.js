import pubsub from 'pubsub';
import keyboard from 'keyboard';
import rivets from 'rivets';
import highcharts from 'highcharts';
'use strict';

var initPlugins = function(factory)
{
    initAccordion(factory.options);
    initHighcharts(factory.options, factory.trns);
};
var initAccordion = function(globals){
    var direction = (globals.config.lang === 'fa')? 'rtl' : 'ltr';
    let icon = (direction == 'rtl') ? 'ui-icon-triangle-1-w' : 'ui-icon-triangle-1-e';
    /* eslint-disable no-undef */
    let thePlugin = $('#'+globals.markup.accordionBaseID).accordion(
        { 
            collapsible:true,  
            multiple:true, 
            heightStyle: 'content',
            icons: {
                'header': icon,
            },
        }
    ).accordion('widget');
    /* eslint-enable no-undef */
    return thePlugin;
    
};

var initHighcharts = function (globals, trns)
{
    highcharts.setOptions({
        global: {
            useUTC: false,
        },
        lang: {
            noData: trns.t('noData'),
        },
    });
    var lineOptions = {
        chart: {
            type: 'line',
            animation: highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {},
        },
        title: {
            text: '',
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150,
        },
        yAxis: {
            title: {
                text: trns.t('usage'),
            },
            plotLines: [{
                value: 0,
                color: '#808080',
            }],
        },
        tooltip: {
            
        },
        
    
        exporting: {
            enabled: false,
        },
        series: [],
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'middle',
            floating:false,
            borderWidth: 1,
        },
    };
    let pieOptions = {
        colors: highcharts.map(highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7,
                },
                stops: [
                    [0, color],
                    [1, highcharts.Color(color).brighten(-0.3).get('rgb')], // darken
                ],
            };
        }),
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            events: {},
            height: 150,
        },
        title: {
            text: '',
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>',
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (highcharts.theme && highcharts.theme.contrastTextColor) || 'black',
                    },
                    connectorColor: 'silver',
                },
            },
        },
        series: [],
    };
    let cpuOptions = lineOptions;
    cpuOptions.chart.events.load = function () {
        pubsub.subscribe('cpuData', addCpuData.bind(this));
        
    };
    cpuOptions.tooltip = {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                highcharts.dateFormat('%H:%M:%S', this.x) + '<br/>' +
                highcharts.numberFormat(this.y, 2)+'<b>%</b>';
        },
    };
    if(globals.modules['cpu'] == true)    
        var cpuChart = highcharts.chart('cpu-chart', cpuOptions);

    let ramOptions = lineOptions;
    ramOptions.chart.events.load = function () {
        pubsub.subscribe('ramData', addRamData.bind(this));
    };
    ramOptions.tooltip = {
        formatter: function () {
            return '<b>' + this.series.name + '</b><br/>' +
                highcharts.dateFormat('%H:%M:%S', this.x) + '<br/>' +
                highcharts.numberFormat(this.y, 2)+'<b></b>';
        },
    };
    ramOptions.title.text = '';
    ramOptions.yAxis.title.text = trns.t('gigabyte');
    if(globals.modules['ram'] == true)
        var ramChart = highcharts.chart('ram-chart', ramOptions);
    

    

 
    // Build the chart
    // highcharts.chart('container', pieOptions);
    if(globals.modules['storages'] == true)
        makeStorageCharts(pieOptions);
    /* eslint-disable no-undef */
    $( '#'+globals.markup.accordionBaseID ).accordion( 'refresh' );
    
    // needs fix - better implementation of openning all accordion tabs is needed
    if(globals.markup.accordionExpandAll)
        $('.ui-accordion-content').show();
    
    /* eslint-enable no-undef */
    console.log(new Date().getTime(), '_Highcharts');
    
};

var addCpuData = function(msg, data){
    let chart = this;

    if (chart.series.length === 0) {
        Object.keys(data).forEach(function(corePoint, i) {
            chart.addSeries({
                data: [],
                name: 'core-'+i,
                id : 'series-core'+i,
            },true);
        });
    }
    if(chart.series.length > 0)
    {
        try {
            Object.keys(data).forEach(function(corePoint, i) {
                var series = chart.get('series-core'+i);
                let shift = series.data.length > 20; // shift if the series is 
        
                // add the point
                data[corePoint][1] = parseInt(data[corePoint][1]);
                series.addPoint(data[corePoint], true, shift);
            });
        } catch (error) {
            console.log(error);
        }
    }
    console.log(new Date().getTime(), '_AddCpu');
    
};
var addRamData = function(msg,data){
    let chart = this;
    if (chart.series.length === 0) {
        Object.keys(data).forEach(function(usageData, i) {
            if(usageData == 'ram')
            {
                chart.addSeries({
                    data: [],
                    name: 'RAM',
                    id : 'series-ram-'+i,
                });
            }
            if(usageData == 'swap')
            {
                chart.addSeries({
                    data: [],
                    name: 'SWAP',
                    id : 'series-swap-'+i,
                });
            }
        });
    }
    if(chart.series.length > 0)
    {
        try {
            Object.keys(data).forEach(function(usageData, i) {
                if(usageData == 'ram')
                {
                    let series = chart.get('series-ram-'+i);
                    let shift = series.data.length > 20; // shift if the series is 
            
                    // add the point
                    series.addPoint(data[usageData], true, shift);
                }
                if(usageData == 'swap')
                {
                    let series = chart.get('series-swap-'+i);
                    let shift = series.data.length > 20; // shift if the series is 
            
                    // add the point
                    series.addPoint(data[usageData], true, shift);
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

};
var addStorageData = function(msg, data){
    let chart = this;
    if (chart.series.length === 0) {
        //
        chart.addSeries({
            data: [
                {name: 'used', y: data.used},
                {name: 'free', y: data.free},
            ],
            name: data.name,
            id : 'series-storage-',
        });
    }
    if(chart.series.length > 0)
    {
        //
        chart.series[0].setData([
            {name: 'used', y: data.used},
            {name: 'free', y: data.free},
        ]);
    }
};
var makeStorageCharts = function(options){
    let storageCharts = document.querySelectorAll('*[id^="storage-chart-"]');
    storageCharts.forEach(function(element){
        options.chart.events.load = function () {
            pubsub.subscribe('storageData'+element.id.substr(element.id.length - 1), addStorageData.bind(this));
        };
        highcharts.chart(element.id, options);
    });
};
export {
    initPlugins,
};   
// {
//     name: 'Brands',
//     data: [
//         { name: 'IE', y: 56.33 },
//         {
//             name: 'Chrome',
//             y: 24.03,
//             sliced: true,
//             selected: true,
//         },
//         { name: 'Firefox', y: 10.38 },
//         { name: 'Safari', y: 4.77 },
//         { name: 'Opera', y: 0.91 },
//         { name: 'Other', y: 0.2 },
//     ],
// }