import pubsub from 'pubsub';
'use strict';

var getFile = function(src, type , callback) {
    var result;
    var _HTTP = new XMLHttpRequest(),
        _self = this;

    if (_HTTP) {
        _HTTP.open('GET', src, true);
        if(type == 'vid'){
            _HTTP.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
            _HTTP.send(null);
        }
        else if (type == 'snd')
        {
            _HTTP.responseType = 'arraybuffer';
            _HTTP.send();
        }
        else if (type == 'gif')
        {
            _HTTP.responseType = 'arraybuffer';
            _HTTP.send();
        }
        else if (type == 'data')
        {
            _HTTP.responseType = 'json';
            _HTTP.send();
        }

        _HTTP.onprogress = function() {
            if(type == 'vid'){
                pubsub.publish('vidLoading', 'loading '+ src);
            }
            else if (type == 'snd')
            {
                pubsub.publish('sndLoading', 'loading '+ src);
            }
            else if (type == 'gif')
            {
                pubsub.publish('gifLoading', 'loading '+ src);
            }
            else if (type == 'data')
            {
                pubsub.publish('dataLoading', 'loading '+ src);
            }
        };

        if (typeof(_HTTP.onload) !== undefined) {
            _HTTP.onload = function() {
                
                if(type == 'vid'){
                    _self.result = JSON.parse(this.responseText);
                    pubsub.publish('vidLoaded', 'loaded '+ src);
                }
                else if (type == 'snd')
                {
                    _self.result = this.response;
                    pubsub.publish('sndLoaded', 'loaded '+ src);
                }
                else if (type == 'gif')
                {
                    _self.result = this.response;
                    pubsub.publish('gifLoaded', 'loaded '+ src);
                }
                else if (type == 'data')
                {
                    _self.result = this.response;
                    pubsub.publish('dataLoaded', 'loaded '+ src);
                }
        
                callback(_self);
                _HTTP = null;
            };
        } else {
            _HTTP.onreadystatechange = function() {
                if (_HTTP.readyState === 4) {
                    if(type == 'vid'){
                        _self.result = JSON.parse(this.responseText);
                        pubsub.publish('vidLoaded', 'loaded '+ src);
                    }
                    else if (type == 'snd')
                    {
                        _self.result = this.response;
                        pubsub.publish('sndLoaded', 'loaded '+ src);
                    }
                    else if (type == 'gif')
                    {
                        _self.result = this.response;
                        pubsub.publish('gifLoaded', 'loaded '+ src);
                    }
                    callback(_self);
                    _HTTP = null;
                }
            };
        }
        return result;
    } else {
        throw('Error loading file.');
    }
};


export {
    getFile,
};
