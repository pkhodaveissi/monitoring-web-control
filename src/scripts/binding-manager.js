import pubsub from 'pubsub';
import rivets from 'rivets';
import prettyByte from 'pretty-byte';

'use strict';
var data;
var _tokenDataLoaded, _tokenDataUpdated;
var initFillData = function(factory){
    _tokenDataLoaded = pubsub.subscribe( 'dataLoaded', fillData.bind(factory) );
    _tokenDataUpdated = pubsub.subscribe( 'dataLoaded', updateData.bind(factory) );
    data = factory.options.data;
};
var updateData = function(){
    let updatedData = this.options.data;
    let trns = this.trns;
    let modules = this.options.modules;
    Object.keys(modules).forEach(function(key) {
        if(modules[key] == true)
        {
            switch (key) {
            case 'host':
                
                data.host = updatedData.host;
                data.host.dnsServers = data.host.dnsServers.split(',');
                break;
            case 'os':
                data.os = updatedData.os;
                break;
            case 'cpu':
                data.cpu = updatedData.cpu;
                exportChartData('cpu', data.cpu, trns);
                break;
            case 'ram':
                data.ram = updatedData.ram;
                exportChartData('ram', data.ram, trns);
                break;
            case 'storages':
                data.storages = updatedData.storages;
                exportChartData('storage', data.storages, trns);
                break;
            case 'nics':
                data.nics = updatedData.nics;
                break;
            case 'functions':
                data.functions = updatedData.functions;
                break;
            }
        }
    });

    
};
var fillData = function(){
    let trns = this.trns;
    let globals = this.options;
    let templates = this.templates;
    let modules = globals.modules;
    data = globals.data; //binded this via pubsub

    Object.keys(modules).forEach(function(key) {
        if(modules[key] == true)
        {
            switch (key) {
            case 'host':
                fillHost(globals);
                break;
            case 'os':
                fillOS(globals);
                break;
            case 'cpu':
                fillCpu(globals);
                
                break;
            case 'ram':
                fillRam(globals);
            
                break;
            case 'storages':
                fillStorage(globals);
            
                break;
            case 'nics':
                fillNic(globals);
                break;
            case 'functions':
                fillFunctions(globals);
                break;
            }
        }
    });
    
  
    pubsub.unsubscribe(_tokenDataLoaded);
    rivets.bind(document.getElementById(this.options.markup.accordionBaseID), this.options.data);
    /* eslint-disable no-undef */
    // //needs fix //should be happening somewhere else end with proper returned value of accordion
    /* eslint-enable no-undef */
    pubsub.publish('rivetsDone', 'done');
};
var fillHost = function(globals,data,trns, template){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-host');
    osContainer.innerHTML = '<host-info-comp class="container-fluid" data="host"></host-info-comp>';
  
};
var fillOS = function(globals,data,trns, template){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-os');
    osContainer.innerHTML = '<os-info-comp class="container-fluid" data="os"></os-info-comp>';
  
};
var fillCpu = function(globals,data, trns){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-cpu');
    osContainer.innerHTML =  '<cpu-info-comp class="container-fluid" data="cpu"></cpu-info-comp>';
};
var fillRam = function(globals,data, trns){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-ram');
    osContainer.innerHTML =  '<ram-info-comp class="container-fluid" data="ram"></ram-info-comp>';
};
var fillStorage = function(globals,data, trns){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-storages');
    osContainer.innerHTML =  '<storages-info-comp class="container-fluid" data="storages"></storages-info-comp>';
};
var fillNic = function(globals,data, trns){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-nics');
    osContainer.innerHTML =  '<nics-info-comp class="container-fluid" data="nics"></nics-info-comp>';
};
var fillFunctions = function(globals,data, trns){
    let osContainer = document.getElementById(globals.markup.accordionBaseID+'-content-functions');
    osContainer.innerHTML =  '<functions-info-comp class="container-fluid" data="functions"></functions-info-comp>';
};
var exportChartData = function (type, data, trns)
{
    switch (type) {
    case 'cpu':
        if(data.cores.length > 0)
        {
            //
            let toExport = {};
            data.cores.forEach(function(core,i){
                let x = (new Date()).getTime(), // current time
                    y = core.load;
                toExport[i] = [x,y];
            });

            pubsub.publish('cpuData', toExport);
        }        
        break;
    case 'ram':
        if(typeof data.capacity == 'string')
        {
            let toExport = {};
            // toExport.capacity = prettyByte(parseInt(data.capacity), true);
            // toExport.free = prettyByte(parseInt(data.free), true);
            // toExport.used = prettyByte(parseInt(data.used), true);
            let x = (new Date()).getTime(),
                y = parseInt(prettyByte(parseInt(data.used),true).value);
            toExport.ram = [x,y];

            let a = x,
                b = parseInt(prettyByte(parseInt(data.swapUsed),true).value);
            toExport.swap = [a,b];
            pubsub.publish('ramData', toExport);
        }        
        break;
    case 'storage':
        if(Array.isArray(data) && data.length > 0)
        {
            data.forEach(function(storage, i){
                let toExport = {};
                toExport.name = prettyByte(parseInt(storage.capacity))+ ' '+ storage.fileSystem;
                toExport.used = (parseInt(storage.used) / parseInt(storage.capacity))*100;
                toExport.free = 100-toExport.used;
                toExport.freeTrns = trns.t('free');
                toExport.usedTrns = trns.t('used');
                pubsub.publish('storageData'+i, toExport);
            });
        }
        break;
    default:
        break;
    }
};
export {
    initFillData,
}; 