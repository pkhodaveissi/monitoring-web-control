/*!
 * @license Samim Co. (2017)
 * Version 0.05.1
 */

import factory from 'factory';
import pubsub from 'pubsub';
import i18nx from 'i18nx';
import enLang from 'nls/en-us/en';
import faLang from 'nls/fa-ir/fa';
import * as schema from 'schema';
import * as bindingMngr from 'binding-manager';
import * as pluginMngr from 'plugin-manager';
import * as fileProccessor from 'file_processor';

// 'ao' is used here as a demonstration
/* eslint-disable no-undef */
window.$ = $;
window.jQuery = $;
var theAccordion, dataProcess;
$.widget( 'samim.monitoringCtrl', {
    
    // Options to be used as defaults
    options: {
        input:{
            data: 'data/monitor.jsonq',
        },
        modules: {
            host: true,
            os: true,
            cpu: true,
            ram: true,
            storages: true,
            nics: true,
            functions: true,
        },
        markup: {
            accordionBaseID: 'monitoringCtrl-accordion',
            accordionExpandAll: false,
        },
        config: {
            dev: false,
            key: atob('MC4wNi4w'),
            lang: 'fa',
            intervalDur: 5000,
        },
    },
    // Set up widget (e.g. create element, apply theming,
    // bind events, etc.)
    _create: function () {
        console.log(new Date().getTime());
        var self = this;
        this._getData(this.options.input.data);
        /// translation object
        this.trns = i18nx.init({
            lng: this.options.config.lang,
            debug: this.options.config.dev,
            ns: ['common'],
            defaultNS: 'common',
            resources: {
                en: {
                    common: enLang,
                },
                fa: {
                    common: faLang,
                },
            },
        }, function(err, t) {
        // init set content
        // updateContent();
        });
        //fix to destroy the plugin in a better way
        schema.init(self);
        bindingMngr.initFillData(self);
        
    },
    //end _create
    _getCreateOptions: function(){
        return {
            'targetId': this.element.attr( 'id' ),
        };
    }, // end _getCreateOptions
    _getData: function(src){
        var self = this;
        
        dataProcess = fileProccessor.loadData(src, self.options.config.intervalDur, function(data){
            self.options.data = data;
        });
    },

    _init: function(){
        let self = this;
        pubsub.subscribe('rivetsDone', self.initPlugins.bind(self));

        console.log(new Date().getTime(), '_init');
        
    },
    initPlugins: function(){
        pluginMngr.initPlugins(this);
    },
    // Destroy an instantiated plugin and clean up modifications
    // that the widget has made to the DOM
    destroy: function () {
        dataProcess.destroy();
        pubsub.clearAllSubscriptions();
        var elem = document.getElementById(this.options.targetId);
        elem.innerHTML = '';
        //t his.element.removeStuff();
        // For UI 1.8, destroy must be invoked from the base
        // widget
        $.Widget.prototype.destroy.call( this );
    // For UI 1.9, define _destroy instead and don't worry
    // about calling the base widget
    },

    
 
   

    //Respond to any changes the user makes to the option method
    _setOption: function ( key, value ) {
        switch (key) {
        case 'someValue':
            //this.options.someValue = doSomethingWith( value );
            break;
        default:
            //this.options[ key ] = value;
            break;
        }

        // For UI 1.8, _setOption must be manually invoked from
        // the base widget
        $.Widget.prototype._setOption.apply( this, arguments );
    // For UI 1.9 the _super method can be used instead
    //this._super( "_setOption", key, value );Accordion Widget
    },

    _destroy: function() {
      
    },


});
setTimeout(function() {
    $(document).trigger({
        type: 'pluginLoaded',
        message: 'pluginLoaded',
        time: new Date(),
    });
    
}, 1000);
/* eslint-enable no-undef */
