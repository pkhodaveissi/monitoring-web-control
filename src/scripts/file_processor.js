// import pubsub from 'pubsub';
import * as fileR from 'file_retriever';
import { clearTimeout } from 'timers';

'use strict';

var loadData = function(src, intervalDur, callback)
{

    let timeout;
    let process = {};
    let resume = true;
    process.fn = function(){
        fileR.getFile(src, 'data', function(data){
            callback(data.result);
            if(resume)
            {
                timeout = setTimeout( process.fn, intervalDur );
            }   
        });
                
    };
    process.kill = function(){
        resume = false;
    };
    process.destroy = function(){
        process.kill();
        window.clearTimeout(timeout);
    };
    process.fn();
    return process;
};


export {
    loadData,
};
