const assert = require('assert');
import prettyByte from '../src/scripts/pretty-byte';

describe('Pretty-Byte', () => {
    it('should produce right time integer', () => {
        var sampleInput = 3758096384;
        var output = parseInt(prettyByte(parseInt(sampleInput), true).value);
        assert.equal(output, 3);
        
    });
});