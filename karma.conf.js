const path = require('path');

module.exports = (config) => {
    const tests = 'tests/*.test.js';
    process.env.BABEL_ENV = 'karma';
    config.set({
        frameworks: ['mocha'],
       
      
        files: [
            {
                pattern: tests,
            },
        ],
        browsers: ['PhantomJS'],
        // Preprocess through webpack
        preprocessors: {
            [tests]: ['webpack'],
        },
        
        singleRun: false,
        webpack: require('./webpack.parts').loadJavaScript({
            include: path.join(__dirname, 'src'),
            postLoaders: [{
                test: /\.js/,
                exclude: /(test|node_modules|bower_components)/,
                loader: 'istanbul-instrumenter',
            }],
        }),
        logLevel: config.LOG_LOG,
        client: {
            captureConsole: true,
            mocha: {
                bail: true,
            },
        },
        reporters: ['progress', 'mocha', 'coverage'],
        coverageReporter: {

            dir: 'build/coverage/',
            reporters: [
                { type: 'html' },
                { type: 'text' },
                { type: 'text-summary' },
            ],
        },
        webpackMiddleware: {
            // webpack-dev-middleware configuration
            noInfo: true,
        },
      
        // reporter options
        mochaReporter: {
            colors: {
                success: 'blue',
                info: 'bgGreen',
                warning: 'cyan',
                error: 'bgRed',
            },
            symbols: {
                success: '+',
                info: '#',
                warning: '!',
                error: 'x',
            },
        },

        browserConsoleLogOptions: {
            level: 'log',
            terminal: true,
        },
    });
};