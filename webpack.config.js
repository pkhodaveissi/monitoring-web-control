const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
// const inProduction = (process.env.NODE_ENV === 'production');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const merge = require('webpack-merge');

const parts = require('./webpack.parts');

const PATHS = {
    src: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'build'),
};

const commonConfig = merge([
    {
        node: {
            fs: 'empty',
        },
        entry: {
            monitoringCtrl: [
                PATHS.src + '/scripts/plugin.js',
                PATHS.src + '/styles/main.scss',
            ],
            // 'frame-player' : PATHS.src + '/scripts/frame-player.js', 
        },
        output: {
            path: PATHS.build,
            publicPath: '',
            filename: 'libs/[name].min.js',
        },
        resolve: {
            modules: ['./src/scripts', './node_modules'],
            alias: {
                'vis-time': '../../node_modules/vis/dist/vis-timeline-graph2d.min',
                'keyboard': '../../node_modules/keyboardjs/dist/keyboard.min',
                'i18nx': '../../node_modules/i18next/i18next.min',
                'pubsub': '../../node_modules/pubsub-js/src/pubsub',
                'factory': '../scripts/jquery-ui.min',
                'highcharts': '../scripts/highcharts',
            },
        },
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/,
                    use: ExtractTextPlugin.extract({
                        use: ['css-loader', 'sass-loader'],
                        fallback: 'style-loader',
                    }),
                },

                /// neccessary to load fonts, otherwise webpack will try to parse the fonts and will throw errors
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        publicPath: '',
                        useRelativePath: true,
                        emitFile:false,
                    },  
                },
                
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    enforce: 'pre',
                    loader: 'eslint-loader',
                    options: {
                        emitWarning: true,
                    },
                },
                // Used to move files to build dir but Using Copy Plugin instead
                // {
                //     test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2|mp4|mp3|json)$/,
                //     use: {
                //         loader: 'file-loader',
                //         options: {
                //           name: '[path][name].[ext]',
                //           outputPath: 'build'
                //         }  
                //     }
                // }
            ],
        },

        externals: {
            jquery: {
                commonjs: 'jQuery',
                amd: 'jQuery',
                root: '$', // indicates global variable
            },
            Konva: {
                commonjs: 'Konva',
                amd: 'Konva',
                root: 'Konva', // indicates global variable
            },
           
        },
        
        plugins: [
            
            new ExtractTextPlugin('css/[name].min.css'),
            
            new CopyWebpackPlugin([
                { from: './src/fonts', to: './fonts' },
                { from: './src/data', to: './data' },
                { from: './src/css', to: './css' },
                { from: './src/css/images', to: './css/images' },
                { from: './node_modules/bootstrap/dist/css/bootstrap.min.css', to: './css' },
                { from: './src/libs', to: './libs' },
                

            ]),
            new webpack.LoaderOptionsPlugin({
                options: {
                    eslint: {
                        // Fail only on errors
                        failOnWarning: false,
                        failOnError: true,
                        exclude: /node_modules/,
                        // Toggle autofix
                        fix: false,

                        // Output to Jenkins compatible XML
                        outputReport: {
                            filePath: 'checkstyle.xml',
                            formatter: require('eslint/lib/formatters/checkstyle'),
                        },
                    },
                },
            }),
        ],

    },
    parts.lintJavaScript({ include: PATHS.src + '/scripts' }),
    parts.loadJavaScript({ include: PATHS.src }),
]);
const productionConfig = merge([
    {
        plugins: [
            new UglifyJSPlugin(
                { 
                    comments: false,
                }),
            new webpack.LoaderOptionsPlugin({
                minimize: true,
            }),
            new webpack.BannerPlugin(
                '@license Samim Co. (2017)\n'+
                'Version 0.01.1'
            ),
        ],
    },
    parts.clean(PATHS.build),
    
]);
const developmentConfig = merge([
    parts.devServer({
        // Customize host/port here if needed
        host: process.env.HOST,
        port: process.env.PORT,
    }),
]);
module.exports = (env) => {
    // if (env === 'production') {
    //     return merge(commonConfig, productionConfig);
    // }

    // return merge(commonConfig, developmentConfig);
    // const pages = [
    //     parts.page({ title: 'Webpack demo', path: '/' }),
    // ];
    // const config = env === 'production' ?
    //     productionConfig :
    //     developmentConfig;
    
    // return pages.map(page => merge(commonConfig, config, page));

    const pages = [
        parts.page({
            title: 'Monitoring Control demo',
            path: './',
            template: require.resolve(
                './src/main_index.ejs'
            ),
            entry: {
                
            },
        }),
        // parts.page({
        //     title: 'Another demo',
        //     path: 'another',
        //     entry: {
        //         another: path.join(PATHS.app, 'another.js'),
        //     },
        // }),
    ];
    const config = process.env.NODE_ENV === 'production' ?
        productionConfig :
        developmentConfig;
    console.log(process.env.NODE_ENV, process.env.NODE_ENV === 'production');
    return pages.map(page => merge(commonConfig, config, page));
    
};