module.exports = {
    env: {
      browser: true,
      commonjs: true,
      es6: true,
      node: true,
      mocha: true,
    },
    extends: 'eslint:recommended',
    "parser": "babel-eslint",
    parserOptions: {
      "sourceType": "module",
      "ecmaVersion": 6,
      "ecmaFeatures": {
        "blockBindings": true,
        "modules": true,
      },
      "allowImportExportEverywhere": true,
    },
    rules: {
      'comma-dangle': ['error', 'always-multiline'],
      indent: ['error', 4],
      'linebreak-style': ['error', 'unix'],
      quotes: ['error', 'single'],
      semi: ['error', 'always'],
      'no-unused-vars': ['off'],
      'no-useless-escape': ['off'],
      'no-console': 0,
      'no-undef': ['warn'],
      'no-constant-condition': ['warn'],
      'no-redeclare': ['warn'],
    },
  };